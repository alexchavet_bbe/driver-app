<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Fulfillment extends Model
{
    //

		public function isComplete()

		{
				return false;
		}

		static public function index($id = false)

		{
				if(!$id){
					return Fulfillment::get();
				}

				return Fulfillment::where('order_id', $id)->get();
		}

		public static function searchTracking($column, $value)
		{

		    $tracking =  Fulfillment::where((string)$column, (string)$value)
										->get()
										->filter(function ($fulfillment, $key) {
											$updated = $fulfillment['updated_at'];
											//$date
											return $updated;
										})
										->map(function ($fulfillment, $key) {
											$updated = $fulfillment['updated_at'];
											$ETA = new Carbon($fulfillment['ETA']);
											 return [
												'tracking' => $updated->diffInMinutes($ETA),
								 				'ETA' => $ETA,
								 				'updated_at' => $updated
											];
										});
			return [
				// 'tracking' => $tracking['updated']->diffInMinutes($tracking['ETA']),
				'tracking' => $tracking,
				$column,
				$value
			];
		}

        public static function search($column, $value)  {
            $tracking =  Fulfillment::where((string)$column, (string)$value)->get();
            return $tracking;
        }
}

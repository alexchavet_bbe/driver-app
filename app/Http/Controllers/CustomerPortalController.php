<?php

namespace App\Http\Controllers;

use App\MyShopify;
use App\Setting;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7;
use Illuminate\Support\Facades\Validator;
use Stripe\Stripe;

class CustomerPortalController extends Controller
{
    //
    private function get($endpoint, $key)
    {
        $client = new Client;
        try {
            $response = $client->request('GET', 'https://api.rechargeapps.com/' . $endpoint, [
                'headers' => [
                    'content-type' => 'application/json',
                    'response-type' => 'json',
                    'x-recharge-access-token' => Setting::getSetting('rechargekey')
                ]
            ]);
            return $key ? json_decode($response->getBody()->getContents(), true)[$key] : json_decode($response->getBody()->getContents(), true);
            //return $response->getBody()->getContents();
        } catch (ClientException $ex) {
            return response()->json($ex->getMessage(), 500);
        }
    }

    private function delete($endpoint, $key)
    {

        $client = new Client;
        try {
            $response = $client->request('DELETE', 'https://api.rechargeapps.com/' . $endpoint, [
                'headers' => [
                    'content-type' => 'application/json',
                    'response-type' => 'json',
                    'x-recharge-access-token' => Setting::getSetting('rechargekey')
                ]
            ]);
            return json_decode($response->getBody()->getContents(), true);
            //return $response->getBody()->getContents();
        } catch (ClientException $ex) {
            return response()->json($ex->getMessage(), 500);
        }
    }

    private function post($endpoint, $body, $key)
    {
        $client = new Client;
        try {
            $response = $client->request('POST', 'https://api.rechargeapps.com/' . $endpoint, [
                'headers' => [
                    'content-type' => 'application/json',
                    'response-type' => 'json',
                    'x-recharge-access-token' => Setting::getSetting('rechargekey')
                ],
                'json' => $body
            ]);
            return json_decode($response->getBody()->getContents(), true)[$key];;
            //return $response->getBody()->getContents();
        } catch (ClientException $ex) {
            return response()->json($ex->getMessage(), 500);
        }
    }

    private function put($endpoint, $body, $key)
    {
        $client = new Client;
        try {
            $response = $client->request('PUT', 'https://api.rechargeapps.com/' . $endpoint, [
                'headers' => [
                    'content-type' => 'application/json',
                    'response-type' => 'json',
                    'x-recharge-access-token' => Setting::getSetting('rechargekey')
                ],
                'json' => $body
            ]);
            return json_decode($response->getBody()->getContents(), true)[$key];;
            //return $response->getBody()->getContents();
        } catch (ClientException $ex) {
            return response()->json($ex->getMessage(), 500);
        }
    }

    public function get_customer($id)
    {

        $endpoint = "customers/?hash=" . $id;
        return $this->get($endpoint, 'customers')[0];
    }

    public function index_customer()
    {

        $endpoint = "subscriptions?limit=250";
        return collect($this->get($endpoint, 'subscriptions'))->map(function ($sub) {
            return [
                'id' => $sub['customer_id'],
                'cus' => $sub,
                'date' => Carbon::parse($sub['next_charge_scheduled_at'])->format('D M d Y'),
                'day' => Carbon::parse($sub['next_charge_scheduled_at'])->format('D')
            ];
        })->groupBy('day');
    }

    public function put_customer($id, Request $request)
    {

        $endpoint = "customers/" . $id;
        return $this->put($endpoint, $request->all(), 'customer');
    }

    public function index_payment($id, Request $request)
    {

        $endpoint = "customers/" . $id . "/payment_sources";
        return $this->get($endpoint, 'payment_sources');
    }

    public function update_card(Request $request)
    {
        try {
//
            Validator::make($request->all(), [
                'customer' => 'required',
                'card' => 'required',
                'customerId' => 'required',
            ])->validate();
            //getAll payment methods of the Customer (RechargeAPI)
            $endpoint = "payment_methods?customer_id=$request->customerId";
            $paymentMethodsList = $this->get($endpoint, 'payment_methods');
            //get the id of customer's default payment method (RechargeAPI)
            $paymentId = null;
            foreach ($paymentMethodsList as $paymentMethod) {
                $paymentMethod = json_decode(json_encode($paymentMethod), FALSE);
                if ($paymentMethod->default) {
                    $paymentId = $paymentMethod->id;
                }
            }
            //add processor_payment_method_token of the new card to customer's default payment method(RechargeAPI)
            $this->put("payment_methods/" . $paymentId,
                [
                    "processor_payment_method_token" => $request->card
                ]
                , "payment_method");

            Stripe::setApiKey(env('STRIPE_API_KEY_PROD', false));
            $payment_method = \Stripe\PaymentMethod::retrieve(
                $request->card
            );
            $payment_method->attach([
                'customer' => $request->customer
            ]);
            \Stripe\Customer::update($request->customer, [
                'invoice_settings' => [
                    'default_payment_method' => $payment_method->id
                ]
            ]);
        } catch (\Exception $exception) {
            abort('500', $exception);
        }
    }


    public function add_subscription(Request $request)
    {

        $endpoint = "subscriptions";
        return $this->post($endpoint, $request->all(), 'subscription');
    }

    public function index_subscriptions($id)
    {

        $endpoint = "subscriptions?customer_id=$id";
        return $this->get($endpoint, 'subscriptions');
    }

    public function index_address($id)
    {

        $endpoint = "customers/$id/addresses/";
        return $this->get($endpoint, 'addresses');
    }


    public function get_address($id)
    {

        $endpoint = "addresses/" . $id;
        return $this->get($endpoint, 'address');
    }

    public function put_address($id, Request $request)
    {

        $endpoint = "addresses/" . $id;
        return $this->put($endpoint, $request->all(), 'address');
    }

    public function index_products()
    {
        $endpoint = "products?limit=250";
        return $this->get($endpoint, 'products');
    }

    public function get_collection($id, MyShopify $myShopify) {
        //getAll Recharge products
        $endpoint = "products?limit=250";
        $productsList = $this->get($endpoint, 'products');
        //getAll Shopify products from the Collection
        $productsOfCollection = $myShopify->get("/admin/api/2020-07/collections/$id/products.json");
        // Create an array includes products_id
        $productsIdList = array();
        foreach ($productsOfCollection as $product){
            $productsIdList[] = $product->id;
        }
        // Show Recharge products in a Collection
        $productsOfCollectionFixed = array();
        foreach ($productsList as $product){
            $productObj = json_decode(json_encode($product), FALSE);
            if(in_array( $productObj->product_id, $productsIdList)){
                $productsOfCollectionFixed[] = $productObj  ;
            }
        }

        return $productsOfCollectionFixed;
    }

    public function index_shopify_products(Request $request, MyShopify $myShopify)
    {
        $products = $myShopify->get('admin/products.json',
            [
                'limit' => 250
            ]);

        return $products;
    }


    public function index_one_time($id)
    {
        $endpoint = "onetimes/?address_id=" . $id;
        return $this->get($endpoint, 'onetimes');
    }

    public function add_one_time($id, Request $request)
    {

        $endpoint = "onetimes/address/$id/";
        return $this->post($endpoint, $request->all(), 'onetime');
    }

    public function delete_one_time($id)
    {

        $endpoint = "onetimes/$id/";
        return $this->delete($endpoint, '');
    }


    public function update_subscription($id, Request $request)
    {
        $endpoint = "subscriptions/" . $id;
        return $this->put($endpoint, $request->all(), 'subscription');
    }

    public function update_bulk_subscription($id, Request $request)
    {
        $endpoint = "addresses/" . $id . "/subscriptions-bulk";
        return $this->put($endpoint, $request->all(), 'subscriptions');
    }

    public function set_next_charge_date($id, Request $request)
    {
        $endpoint = "subscriptions/" . $id . "/set_next_charge_date/";
        return $this->post($endpoint, $request->all(), 'subscription');
    }

    public function index_charges($id)
    {
        $endpoint = "charges?customer_id=" . $id;
        return $this->get($endpoint, 'charges');
    }

    public function index_orders($id)
    {
        $endpoint = "orders?customer_id=" . $id;
        return $this->get($endpoint, 'orders');
    }


    public function delete_subscription($id)
    {
        $endpoint = "subscriptions/$id/";
        return $this->delete($endpoint, '');
    }

    public function get_all_collection(Request $request, MyShopify $myShopify)
    {
        $collection = $myShopify->get('/admin/api/2020-07/collects.json');

        return $collection;
    }

    public function collectionList(Request $request, MyShopify $myShopify)
    {
        //get SmartCollection, convert to array
        $smartCollection = $myShopify->get('/admin/api/2020-07/smart_collections.json');
        $smartCollectionArr = json_decode(json_encode($smartCollection), true);
        //get CustomCollection, convert to array
        $customCollection = $myShopify->get('/admin/api/2020-07/custom_collections.json');
        $customCollectionArr = json_decode(json_encode($customCollection), true);
        //get list All Collection = smartCollection + customCollection
        $collectionList = array_merge( $smartCollectionArr, $customCollectionArr);
        //Apply filter on $collectionList => metafiled: show_on_page === true. Return result
        $collectionListShow = array();
        foreach ($collectionList as $collection){
            //Convert $collection to Objects
            $collectionObj = json_decode(json_encode($collection), FALSE);
            //GetAll $metafieldList attached to the $collection
            $metafieldList = $myShopify->get("/admin/collections/$collectionObj->id/metafields.json");
            //Convert the list to array
            $metafieldListArr = json_decode(json_encode($metafieldList), true);
            foreach ($metafieldListArr as $metafield){
                //convert metafield to Objects
                $metafieldObj = json_decode(json_encode($metafield), FALSE);
                // Filter. If the collection has metafield that have key=show_on_page and value=true, add the collection to $collectionListShow
                ($metafieldObj->key == "show_on_page" && $metafieldObj->value == true) ?  $collectionListShow[] = $collectionObj:"";
            }
        }
        return $collectionListShow;
    }

    public function create_checkout(Request $request)
    {
        $endpoint = "checkouts/";
        return $this->post($endpoint, $request->all(), 'checkout');
    }

    public function update_checkout($id, Request $request)
    {
        $endpoint = "checkouts/" . $id;
        return $this->put($endpoint, $request->all(), 'checkout');
    }

    public function retrieve_checkout($id, Request $request)
    {
        $endpoint = "checkouts/" . $id;
        return $this->get($endpoint, 'checkout');
    }
}


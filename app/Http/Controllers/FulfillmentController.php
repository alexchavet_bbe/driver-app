<?php

namespace App\Http\Controllers;

use App\Fulfillment;
use App\MyShopify;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

use GuzzleHttp\Client;

class FulfillmentController extends Controller
{
    public function index($id)
    {
        $fulfillments = Fulfillment::index($id);

        return $fulfillments;
    }

    public function show($id)
    {
        $fulfillments = Fulfillment::index($id);

        return $fulfillments;
    }

    public function search(string $column, string $value)
    {
        $fulfillments = Fulfillment::search($column, $value);

        return $fulfillments;
    }

    public function create($request)
    {
        $fulfillments = Fulfillment::index($id);
        return $fulfillments;
    }

    public function summarize($id)
    {
        $date =  Setting::where('label', 'date')->first()->value;
        return  Fulfillment::search('driver_id', $id)
        ->filter(function ($v) use ($date) {
            $v['created_at'] = new Carbon($v['created_at']);
            return $v['created_at'] > new Carbon($date);
        })->values()->all();
    }

    /**
     * @param $id
     * @param MyShopify $myShopify
     */
    public function fix($id, MyShopify $myShopify)
    {

        $fulfillments = Fulfillment::search('driver_id', $id)
            //->pluck('order_id')
            ->sortByDesc('created_at')
            ->map(function ($value) use ($myShopify) {
                $fulfillment_date = $value->created_at;
                $optimo_eta = app('App\Http\Controllers\OptimoRouteController')
                    ->getETA($value->order_id);
                $optimo_eta_collection = collect($optimo_eta);

                if( $optimo_eta_collection->has('scheduleInformation') ){
                    $optimo_eta_hour = explode(':', $optimo_eta['scheduleInformation']['scheduledAt'])[0];
                    $optimo_eta_minute = explode(':', $optimo_eta['scheduleInformation']['scheduledAt'])[1];
                }
                else {
                    $optimo_eta_hour = 0;
                    $optimo_eta_minute = 0;
                }

                $c_new_eta = new Carbon($fulfillment_date);
                $new_eta = $c_new_eta->setTime($optimo_eta_hour, $optimo_eta_minute, 0);

                $to_save = Fulfillment::where('fulfillment_id', $value->fulfillment_id)->first();

                $to_save->ETA = $new_eta;
                $to_save->save();

                return [
                    'order_id' => $value->order_id,
                    'orginal_eta' => $value->ETA,
                    'fulfillment_id' => $value->fulfillment_id,
                    'fulfillment_date' => new Carbon($fulfillment_date),
                    'new_eta' => $new_eta,
                    'optimo_eta' => $optimo_eta
                ];

            });

        dd($fulfillments);

    }

}

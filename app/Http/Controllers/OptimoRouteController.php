<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Setting;
use App\PickupPoint;
use App\MyShopify;

class OptimoRouteController extends Controller
{
	/*
	* returns Collection
	*/
	private function key(){
			return Setting::getSetting('optimokey');
	}
	/*
	* returns Collection
	*/
	public function deleteOrders(Client $client)
	{
		$date = Setting::getDate()['today_date']->format('Y-m-d');
		$data = [
			'date' => $date
		];
		$opt_delete_orders = $client->request(
			'POST',
			"https://api.optimoroute.com/v1/delete_all_orders?key=".$this->key(),
			['json' => $data]
		);
		return json_decode($opt_delete_orders->getBody()->getContents(), true);
	}
	/*
	* returns Collection
	*/
	public function postRequestOrder(Request $request, Client $client)
	{
        $order = json_decode(json_encode($request['body']), FALSE);;

        return $this->postOrder($order, $client);
    }

	public function postOrder($order, Client $client)
	{
		$opt_create_order = $client->request(
			'POST',
			"https://api.optimoroute.com/v1/create_order?key=".$this->key(),
			['json' => $this->getOptimoBody($order) ]
		);
		$opt_create_order_result = json_decode($opt_create_order->getBody()->getContents(), true);

		return  $opt_create_order_result;
	}
	/*
	* returns Collection
	*/
	public function testOrder(Request $request, Client $client)
	{
		$order = $request['order'];

		$date = Setting::getDate()['today_date']->format('Y-m-d');
		$locationNo = (string) $order['customer']['id'];
		$locationName = $order['shipping_address']['first_name'].' '.$order['shipping_address']['last_name'];
		$body = [
				'operation' => 'CREATE',
				'orderNo' => (string) $order['id'],
				'acceptDuplicateOrderNo' => false,
				'type' => 'D',
				'date' => $date,
				'location' => [
					'address' =>  $request['shipping_address'],
					'acceptPartialMatch' => $request['force'],
					'locationNo' =>  $locationNo,
					'locationName' => $locationName
				],
				'duration' => 4,
				'twFrom' => '08:00',
				'twTo' => '18:00'
		];

		$opt_create_order = $client->request(
			'POST',
			"https://api.optimoroute.com/v1/create_order?key=".$this->key(),
			['json' => $body ]
		);
		$opt_create_order_result = json_decode($opt_create_order->getBody()->getContents(), true);

		return  $opt_create_order_result;
	}
	/*
	* returns Collection
	*/
	public function getShippingAddress($order)
	{
		$shipping_address = $order->shipping_address->address1
		.' '.
		$order->shipping_address->address2
		.' '.$order->shipping_address->city
		.' '.$order->shipping_address->zip;

		return $shipping_address;
	}
	/*
	* returns Collection
	*/
	public function getOptimoBody($order)
	{
		$date = Setting::getDate()['today_date']->format('Y-m-d');
		$locationNo = (string) $order->customer->id;
		$locationName = $order->shipping_address->first_name.' '.$order->shipping_address->last_name;
		return [
				'operation' => 'CREATE',
				'orderNo' => "$order->id",
				'acceptDuplicateOrderNo' => false,
				'type' => 'D',
				'date' => $date,
                'load1' => 1,
				'notificationPreference' => 'both',
				'email' => $order->email,
				'phone' => $order->shipping_address->phone,
				'location' => [
					'address' => $this->getShippingAddress($order),
					'locationNo' => $locationNo,
					'locationName' => $locationName
				],
				'duration' => 4,
				'twFrom' => '08:00',
				'twTo' => '18:00',
                'notes' =>  $order->note ?? "",
                'customField1' =>  $order->name  ?? "",
			];
	}

	public function postOrders(Request $request, MyShopify $myShopify, Client $client)
	{
			$delete = $this->deleteOrders($client);

			$orders = app('App\Http\Controllers\ShopifyController')->getOrders($request,$myShopify,$client)['orders_delivery'];

			$optimo_orders = collect($orders)//->splice(0, 10)
			->map(function($item,$key) use ($client) {
				$opt_create_order_result = $this->postOrder($item, $client);
				return [
					'result' => $opt_create_order_result,
					'order' => $item
				];
			})
			->ToArray();
			$optimo_orders_errors = collect($optimo_orders)->filter(function($item,$key) {
						return !$item['result']['success'];
			})->all();

			return response()->json($optimo_orders_errors);
	}
	/*
	* returns Collection
	*/
	public function getRoutes(Client $client)
	{
		$date = Setting::getDate()['today_date']->format('Y-m-d');
		$opt_routes = $client->request(
			'GET',
			"https://api.optimoroute.com/v1/get_routes?key=".$this->key()."&date=".$date
		);

		return json_decode($opt_routes->getBody()->getContents(), true);
	}
	/*
	* returns Collection
	*/
	public function getSchedule($order_id)
	{
        $client = new \GuzzleHttp\Client();

		$scheduling = $client->request(
			'GET',
			"https://api.optimoroute.com/v1/get_scheduling_info?key=".$this->key()."&orderNo=".$order_id
		);

		return json_decode($scheduling->getBody()->getContents(), true);
	}

    public function getETA($order_id)
    {
        $client = new \GuzzleHttp\Client();

        $scheduling = $client->request(
            'GET',
            "https://api.optimoroute.com/v1/get_scheduling_info?key=".$this->key()."&orderNo=".$order_id
        );

        $response = json_decode($scheduling->getBody()->getContents(), true);
        return $response;
    }
}

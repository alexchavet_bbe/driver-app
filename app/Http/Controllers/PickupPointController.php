<?php

namespace App\Http\Controllers;
use Oseintow\Shopify\Shopify;
use Illuminate\Http\Request;

use GuzzleHttp\Client;
use App\PickupPoint;

use App\Http\Requests\StorePickupPointRequest;

class PickupPointController extends Controller
{
    public function index() {
			$pickup_points = PickupPoint::all();
	    return view('admin.pickup-points', compact('pickup_points'));
		}

		public function store(StorePickupPointRequest $request) {

			PickupPoint::create($request->all());

			return redirect()
						->back()
						->with('status', "PickupPoint	 successfully added.");
		}

    public function update(StorePickupPointRequest $request, $id)
    {

				$setting = PickupPoint::where('id', $id);
				$setting->update($request->all());

				return "PickupPoint $request->label successfully added.";
    }

		public function delete($id)
		{

			$setting = PickupPoint::where('id', $id);
			$setting->delete();

			return "PickupPoint  successfully deleted.";
		}

}

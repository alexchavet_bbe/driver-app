<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Setting;

class RechargeController extends Controller
{
		public function index() {
			$response = $this->getCustomers();

			$customers = json_encode(collect($response)->get('customers'));

			return view('recharge.address-list', compact('customers'));
		}


		private function get($endpoint)
    {
        $client = new Client;
        try {
            $response = $client->request('GET', 'https://api.rechargeapps.com/' . $endpoint, [
                'headers' => [
                    'content-type' => 'application/json',
                    'response-type' => 'json',
                    'x-recharge-access-token' => Setting::getSetting('rechargekey')
                ]
            ]);
            return json_decode($response->getBody()->getContents(), true);
            //return $response->getBody()->getContents();
        } catch (HttpException $ex) {
            echo $ex;
        }
    }

		private function put($endpoint, $body)
    {
				$client = new Client;
				try {
					$response = $client->request('PUT', 'https://api.rechargeapps.com/'.$endpoint, [

                        'headers' => [
                            'content-type' => 'application/json',
                            'response-type' => 'json',
                            'x-recharge-access-token' =>  Setting::getSetting('rechargekey')
                        ],
                        'json' => $body
					]);
						return json_decode($response->getBody()->getContents(), true);
						//return $response->getBody()->getContents();
        }
				catch (HttpException $ex) {
            echo $ex;
        }
    }

		// GET /customer/
		public function getCustomers()
    {
				$endpoint = "customers/?limit=250";
				return $this->get($endpoint);
    }

		// GET /customers/<id>
		public function getCustomer($id)
    {
				$endpoint = "customers/".$id;
				return $this->get($endpoint);
    }

		// GET /customers/<id>
		public function getShopifyCustomer($id)
    {
				$endpoint = "customers?shopify_customer_id=".$id;
				return $this->get($endpoint);
    }

		// GET /customers/<id>
		public function listCustomersAddresses()
    {
				$endpoint =  "customers/addresses/";
				return $this->get($endpoint);
    }

		// GET /customers/<id>
		public function getCustomerAddresses($id)
    {
				$endpoint =  "customers/".$id."/addresses/";
				return $this->get($endpoint);
    }

		// PUT /customers/<id>
		public function updateCustomerAddress(Request $request)
    {
				$endpoint =  "addresses/".$request['id'];
				//return $request['address'];
				return $this->put($endpoint, $request['address'] );
    }

		// GET /customers/<id>
		public function getCustomerAddress($c_id, $a_id)
    {
				$endpoint = "/customers/".$c_id."/addresses/".$a_id;
				return $this->get($endpoint);
    }

    // GET /customers/<id>
    public function findCustomer($email)
    {
        $endpoint = "/customers/?email=" . $email;
        return $this->get($endpoint);
    }


}

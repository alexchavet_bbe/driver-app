<?php

namespace App\Http\Controllers;

use App\School;
use App\Setting;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class SchoolCheckoutController extends Controller
{
    /**
     * @param $school
     * @return array
     */
    public function body()
    {
        $school = School::all()->first()->attributesToArray();
        $school['first_name'] = "Alex";
        $school['last_name'] = "Chavet";
        return [
            "shipping_address" => $school,
            "line_items" => [[
                "quantity" => 1,
                "price" => "1.00",
                "variant_id" => 34367759245,
                "product_id" => 9383017933
            ]]
        ];
    }

    public function create()
    {
//        dd($this->body());
        $client = new Client;
        try {
            $response = $client->request('POST', 'https://api.rechargeapps.com/checkouts', [
                'headers' => [
                    'content-type' => 'application/json',
                    'response-type' => 'json',
                    'x-recharge-access-token' => Setting::getSetting('rechargekey')
                ],
                'json' => $this->body()
            ]);
            return json_decode($response->getBody()->getContents(), true);
            //return $response->getBody()->getContents();
        } catch (HttpException $ex) {
            echo $ex;
        }
    }
}

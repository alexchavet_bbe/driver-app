<?php

namespace App\Http\Controllers;

use App\Http\Requests\SchoolRequest;
use App\School;
use Illuminate\Http\Request;

class SchoolController extends Controller
{

    public function index() {
        return School::all()->map(function($school) {
            return $school->attributesToArray();
        });
    }

    public function show()
    {
        $schools = School::all();
        $fields =  $schools->first()->attributesToArray();
        return view('admin.schools', compact('schools', 'fields'));
    }

    public function store(SchoolRequest $request)
    {

//        dd($request->all());

        School::create($request->all());

        return redirect()
            ->back()
            ->with('status', "School successfully added.");
    }

    public function update(SchoolRequest $request, $id)
    {

        $setting = School::where('id', $id);
        $setting->update($request->all());

        return "School $request->label successfully added.";
    }

    public function delete($id)
    {

        $setting = School::where('id', $id);
        $setting->delete();

        return "School  successfully deleted.";
    }
}

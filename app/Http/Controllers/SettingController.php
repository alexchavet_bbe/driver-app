<?php

namespace App\Http\Controllers;
use Oseintow\Shopify\Shopify;
use Illuminate\Http\Request;

use GuzzleHttp\Client;
use App\Setting;

use App\Http\Requests\StoreSettingRequest;

class SettingController extends Controller
{
    public function index() {
			$settings = Setting::all();
	    return view('settings', compact('settings'));
		}

		public function store(StoreSettingRequest $request) {

			Setting::create($request->all());

			return redirect()
						->back()
						->with('status', "Setting	 successfully added.");
		}

    public function update(StoreSettingRequest $request, $id)
    {

				$setting = Setting::where('id', $id);
        $setting->update($request->all());

				return "Setting $request->label successfully added.";
    }

		public function delete($id)
		{

			$setting = Setting::where('id', $id);
			$setting->delete();

			return "Setting  successfully deleted.";
		}

}

<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Oseintow\Shopify\Shopify;
use GuzzleHttp\Client;
use DateTime;
use App\Setting;
use App\PickupPoint;
use App\MyShopify;
use App\Fulfillment;

class ShopifyController extends Controller
{

    public $total_order;

    /*
    * returns Collection
    */
    public function getProducts(Request $request, MyShopify $myShopify)
    {
        $products = $myShopify->get('admin/products.json');

        $products->each(function ($product) {
            \Log::info($product->title);
        });

        return $products;
    }

    /*
    * returns Collection
    */
    public function getOrder(Request $request, MyShopify $myShopify, $id)
    {
        $order = $request;

        $order = $myShopify->get('admin/orders/' . $id . '.json');


        return $order;

    }

    /*
    * returns Collection
    */
    public function updateOrder(Request $request, MyShopify $myShopify)
    {
        $order = $request;


        $body = [
            'order' => [
                'id' => $request['id'],
                'shipping_address' => $request['shipping_address']
            ]
        ];


        $order = $myShopify->put('admin/orders/' . $request['id'] . '.json', $body);


        return $order;
    }


    /*
    * returns Collection
    */
    public function getFulfillments()
    {
        return Fulfillment::all();
    }

    /*
	* returns Collection
	*/

    public function getFulfillmentDate($id, $f_id, MyShopify $myShopify)
    {
        $args = [];

        $getFullmentRest = '/admin/orders/' . $id . '/fulfillments/' . $f_id . '.json';
        return $myShopify->get($getFullmentRest, $args)['created_at'];
    }

    /*
    * returns Collection
    */
    public function fulfill(Request $request, MyShopify $myShopify)
    {
        // location_id must be specified when creating fulfillments
        $order = $request['order'];
        $driver = 'DRIVER--' . $request['driver'];


        $body = [
            'fulfillment' => [
                'tracking_number' => $driver,
                'notify_customer' => true,
                'notify_customer' => true,
                'location_id' => 36940877,
            ]
        ];

        $order_id = $order['id'];

        $response = $myShopify->post('/admin/orders/' . $order_id . '/fulfillments.json', $body);

        $ETA = Carbon::parse($request['eta']);
        $fulfillment = new Fulfillment;
        $fulfillment->order_number = $order['order_number'];
        $fulfillment->order_id = $order_id;
        $fulfillment->fulfillment_id = $response    ['id'];
        $fulfillment->fulfillment_status = true;
        $fulfillment->driver_id = $request['driver'];
        $fulfillment->line_items_count = count($order['line_items']);
        $fulfillment->ETA = $ETA;
        $fulfillment->save();


        return $response;
    }

    /*
    * returns Collection
    */
    public function unfulfill(Request $request, MyShopify $myShopify)
    {
        $response = $myShopify->post('/admin/orders/' . $request['id'] . '/fulfillments/' . $request['fulfillments_id'] . '/cancel.json');

        Fulfillment::where('order_id', $request['id'])
            ->update(['fulfillment_status' => 0]);

        return $response;
    }

    /*
    * returns Collection
    */
    public function updateFulfillementEvent(Request $request, MyShopify $myShopify)
    {
        $order = $request;


        $body = [
            'fulfillment' => [
                'tracking_number' => $request['driver'],
                'notify_customer' => true
            ]
        ];


        $order = $myShopify->post('/admin/orders/' . $request['id'] . '/fulfillments/events.json', $body);


        return $order;
    }

    /*
    * returns Collection
    */

    public function toTotals($orders)
    {

        return collect($orders)
            ->filter(function ($value, $key) {
                return $this->filterWeek($value);
            })
            ->pluck('line_items')
            ->flatten()
            ->groupBy('variant_id')
            ->map(function ($variant, $key) {
                $name = $variant->min('title');
                //var_dump($variant);

                $id = $variant->first()->product_id;

                $children = $variant
                    ->groupBy('title')
                    ->map(function ($child, $key) {
                        $total = $child
                            ->reduce(function ($carry, $item) {
                                return $carry + $item->quantity;
                            });
                        return [
                            'total' => intval($total),
                            'name' => $child->first()->title
                        ];
                    })
                    ->sortByDesc('total')
                    ->toArray();

                $total = $variant
                    ->reduce(function ($carry, $item) {
                        return $carry + $item->quantity;
                    });

                return [
                    'name' => $name,
                    'id' => $id,
                    'children' => $children,
                    'total' => $total
                ];
            })
            ->sortBy('id');
    }

    public function getItems(Request $request, MyShopify $myShopify, Client $client)
    {
        $orders = $this->getOrders($myShopify, $client);

        return [
            'items' => $this->toTotals($orders['orders']),
            'items_pickup' => $orders['orders_pickup'],
            'items_driver' => $orders['orders_driver'],
            'items_delivery' => $this->toTotals($orders['orders_delivery'])
        ];
    }

    private function filterWeek($value)
    {

        $this_week = Setting::where('label', 'week');
        $next_week = 0;
        if ($this_week->count() > 0) {
            $next_week = $this_week->first()->value + 1;
        }
        $haystack = collect($value)->get('tags') || '';
        $needle = 'WK#' . $next_week;

        if (strpos($haystack, $needle) !== false) {
            return false;
        }
        return true;
    }

    public function getOrdersArgs($since_id = false)
    {
        $dates = Setting::getDate();

        return [
            'since_id' => $since_id,
            'limit' => 250,
            'fulfillment_status' => 'unshipped',
            'created_at_max' => $dates['date_max']->format(DateTime::ATOM)
        ];
    }

    public function getFilteredOrders(MyShopify $myShopify, Client $client)
    {
        $orders = [];
        $last_id = false;
        $last_order_count = 250;

        while ($last_order_count === 250) {
            $temp_orders = $myShopify->get('admin/api/2020-04/orders.json', $this->getOrdersArgs($last_id));
            $last_order_count = count($temp_orders);
            $last_id = collect($temp_orders)->last()->id;
            array_push($orders, $temp_orders->all());
        }

        return collect($orders)
            ->flatten()->filter(function ($value, $key) {
                $tag = Setting::where('label', 'orderTag')->first()->value;
                return $tag ? $this->filterByTag($value) : $this->filterDeliveryDay($value);

            });
    }

    public function filterByTag($order) {
        $tag = Setting::where('label', 'orderTag')->first()->value;
        
        if(!$tag) return true;

        return str_contains($order->tags, $tag);
    }

    public function filterDeliveryDay($order)
    {
        //        If pickup, exit now`

        $attr = $order->note_attributes ?? false;

//        if($attr && collect($attr)->filter(function ($value) {
//                return $value->name === 'Pic  kup' && $value->value === "1";
//        })->count()  === 0) {
//            return false;
//        }

        $day = Setting::where('label', 'deliveryday')->first()->value;

        if ($day === 'Saturday' && !$attr) {
            return true;
        }

        if ($day === 'Saturday' && $order->note_attributes) {
            return !collect($attr)->filter(function ($value, $key) {
                    return $value->name === 'Delivery Day' && ($value->value === "Thursday" || $value->value === "Wednesday");
                })->count() > 0;
        }

        if ($day === 'Thursday' && $attr) {
            return collect($attr)->filter(function ($value, $key) {
                    return $value->name === 'Delivery Day' && ($value->value === "Thursday" || $value->value === "Wednesday");
                })->count() > 0;
        }
    }

    public function getDeliveryOrders($orders = null, MyShopify $myShopify, Client $client)
    {
        $pickups_postcodes = PickupPoint::getAllPostcodes();
        $orders = $orders ? $orders : $this->getFilteredOrders($myShopify, $client);
        return
            $orders
                ->filter(function ($value, $key) {
                    return $this->filterWeek($value);
                })
                ->filter(function ($order, $key) use ($pickups_postcodes) {
                    $postcode = $order->shipping_address->zip;
                    return !in_array($postcode, $pickups_postcodes);
                })
                ->flatten()->all();
    }

    public function getPickupOrders($orders = null, MyShopify $myShopify, Client $client)
    {
        $pickups = PickupPoint::getPickupPoints();
        $orders = $orders ? $orders : $this->getFilteredOrders($myShopify, $client);
        return
            $pickups
                ->map(function ($pickup) use ($orders) {
                    $postcodes = explode(",", $pickup->postcodes);
                    $filtered_orders = $orders->filter(function ($order, $key) use ($postcodes) {
                        $postcode = $order->shipping_address->zip;
                        return in_array($postcode, $postcodes);
                    })->flatten()->all();
                    // dd($filtered_orders);
                    return [
                        'name' => $pickup->label,
                        'slug' => $pickup->slug,
                        'postcodes' => $postcodes,
                        'orders' => $filtered_orders,
                        'totals' => $this->toTotals($filtered_orders)
                    ];
                });
    }

    public function getDriverOrders($orders = null, MyShopify $myShopify, Client $client)
    {
        $routes = app('App\Http\Controllers\OptimoRouteController')->getRoutes($client);
        $orders = $orders ? $orders : $this->getFilteredOrders($myShopify, $client);

        return collect($routes['routes'])
            ->filter(function ($value, $key) {
                return $this->filterWeek($value);
            })
            ->map(function ($item, $key) use ($orders) {
                $stops = collect($item['stops'])->map(function ($item, $key) use ($orders) {
                    return $item['orderNo'];
                });

                $driver_orders = $stops
                    ->map(function ($item, $key) use ($orders) {
                        return $orders->where('id', $item)->first();
                    })
                    ->filter(function ($item, $key) {
                        return $item !== null;
                    });

                return [
                    'name' => $item['driverName'],
                    'stops' => $stops,
                    'driver_orders' => $driver_orders->flatten()->all(),
                    'totals' => $this->toTotals($driver_orders)
                ];
            });
    }

    public function getOrders(MyShopify $myShopify, Client $client)
    {

        $orders = $this->getFilteredOrders($myShopify, $client);

        $orders_pickup = $this->getPickupOrders($orders, $myShopify, $client);

        $orders_driver = $this->getDriverOrders($orders, $myShopify, $client);

        $order_delivery = $this->getDeliveryOrders($orders, $myShopify, $client);

        return [
            'orders' => $orders,
            'orders_pickup' => $orders_pickup,
            'orders_delivery' => $order_delivery,
            'orders_driver' => $orders_driver,

        ];
    }

    /*
    * returns Collection
    */
    public function getOrdersExceptions(Request $request, MyShopify $myShopify)
    {
        $dates = Setting::getDate();

        $args = [
            'limit' => 250,
            'query' => [
                'fulfillment_status' => 'unshipped',

                'created_at_min' => $dates['date_min_exceptions']->format("Y-m-d")
            ]
        ];
        $orders = $myShopify->get('admin/orders.json', $args);

        // $orders->each(function($product){
        // 		 \Log::info($product->title);
        // });

        return $orders;
    }

    /*
   * returns Collection
   */
    public function getCustomerId(Request $request, MyShopify $myShopify)
    {

        $email = $request['email'];

        $args = [
            'limit' => 250,
            'query' => 'email:' . $email,
            'fields' => 'id,email'
        ];

        $orders = $myShopify->get('/admin/customers/search.json', $args);

        if (count($orders) === 0) {

            abort(404, 'Email not valid');

        }

        $customer_id = collect($orders)->first()->id;

        return route('public-tracking-index', $customer_id);
    }
}

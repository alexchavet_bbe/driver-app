<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\MyShopify;

class TrackingAPIController extends Controller
{
    private function getDeliveryDate($order)
    {
        $attr = $order->note_attributes ?? false;

        if (!$attr) {
            return 6;
        }

        return collect($attr)->filter(function ($value, $key) {
            return $value->name === 'Delivery Day' && ($value->value === "Thursday" || $value->value === "Wednesday");
        })->count() > 0 ? 3 : 6;
    }

    public function index($id, MyShopify $myShopify)
    {

        $scope = $this;
        $args = [
            'limit' => 5
        ];
        $orders = $myShopify->get('/admin/customers/' . $id . '/orders.json', $args);

        return $orders->sortByDesc('created_at');
    }
}

<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Oseintow\Shopify\Shopify;
use App\MyShopify;
use App\Fulfillment;
use App\Tracking;
use Illuminate\Http\Request;

use GuzzleHttp\Client;
use function PHPSTORM_META\map;

class TrackingController extends Controller
{
    private function getDeliveryDate($order)
    {
        $attr = $order->note_attributes ?? false;

        if (!$attr) {
            return 6;
        }

        return collect($attr)->filter(function ($value, $key) {
            return $value->name === 'Delivery Day' && ($value->value === "Thursday" || $value->value === "Wednesday");
        })->count() > 0 ? 3 : 6;
    }

    public function index($id, MyShopify $myShopify)
    {

        $scope = $this;
        $view = 'public.tracking.index';
        $args = [
            'limit' => 5
        ];
        $orders = $myShopify->get('/admin/customers/' . $id . '/orders.json', $args);

        $orders = collect($orders)->map(function ($order) use ($scope) {
            $day = $scope->getDeliveryDate($order);

            if ($day === 6) {
                $difference = $day - Carbon::parse($order->created_at)->dayOfWeek;
                $order->delivery_date = Carbon::parse($order->created_at)->addDays($difference);

            } else {
                $difference = $day - Carbon::parse($order->created_at)->dayOfWeek;
                $order->delivery_date = Carbon::parse($order->created_at)->addWeek(1)->addDays($difference);
            }

            return $order;
        });

        $customer = $orders->first() ? $orders->first()->customer :  $myShopify->get('/admin/customers/'. $id . '.json')->all();

        return view($view, [
            'orders' => $orders->sortByDesc('created_at'),
            'customer' => (object) $customer
        ]);
    }

    public function show($customer_id, $order_id, Client $client, MyShopify $myShopify)
    {
        $view = 'public.tracking.show';

        $order = $myShopify->get('/admin/orders/' . $order_id . '.json');

        $customer = $order['customer'];

        return view($view, [
            'order' => $order,
            'customer' => $customer
        ]);
    }

    public function get($driver_serial)
    {
        return Tracking::getTrackings($driver_serial);
    }

}

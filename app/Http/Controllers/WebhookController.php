<?php

namespace App\Http\Controllers;

use App\MyShopify;
use App\Setting;
use App\Webhook;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WebhookController extends Controller
{
    /**
     * @param $tags
     * @return bool
     */

    //    OKAY SO HERE WE WANT TO TAG FIRST ORDERS


    private function isCustomerTagged($tags)
    {
        return $tags->filter(function ($value, $key) {
                return $value === 'Saturday' || ($value === 'Thursday' || $value === 'Wednesday');
            })->count() > 0;
    }

    private function getDay($order)
    {
        if (!$order->has('note_attributes')) return 'Saturday';

        $notes = collect($order->get('note_attributes'));

        $delivery_day = (object) $notes->where('name', 'Delivery Day')->first();

        return $delivery_day->value ?? 'Saturday';
    }

    /**
     * @param Request $request
     * @param MyShopify $myShopify
     * @param Client $client
     * @return \Illuminate\Support\Collection
     */
    public function orderCreated(Request $request, MyShopify $myShopify, Client $client)
    {
        $order = collect($request->all());
        $this->tagOrder($request, $myShopify, $client, $order);
    }

    /**
     * @param Request $request
     * @param MyShopify $myShopify
     * @param Client $client
     * @param $order
     * @return mixed
     */
    public function tagOrder(Request $request, MyShopify $myShopify, Client $client, $order) {
        $order_id = $order->get('id');
        $customer = (object) $order->get('customer');

        $day = $this->getDay($order);

        $order_tags = collect(explode(', ', $order['tags']));

        $webhook = new Webhook();
        $webhook->webhook_id = $order_id;

        try  {
            $response = $myShopify->put('/admin/api/2020-01/orders/' . $order_id . '.json', [
                'order' => [
                    'id' => $order_id,
                    'tags' => $order_tags->push($day)->values()
                ]
            ]);

            $customer_tags = collect(explode(', ', $customer->tags));
            $response = $myShopify->put('/admin/api/2020-01/customers/' . $customer->id . '.json', [
                'customer' => [
                    'id' =>  $customer->id,
                    'tags' => $customer_tags->push($day)->values()
                ]
            ]);
            $webhook->status = $response['tags'];
            $webhook->save();
            return $response;
        } catch (\Exception $exception) {
            $webhook->status = "500";
            $webhook->error = $exception->getMessage();
            $webhook->save();
        }

    }

    public function orderAll(Request $request, MyShopify $myShopify, Client $client)
    {
        $count = 0;
        $that = $this;
        function call($that, $myShopify, $request, $client, $count, $since = false) {
            $orders = $myShopify->get('/admin/api/2020-01/orders.json', [
                'fields' => 'fields=test,customer,note_attributes,id,tags',
                'limit' => 20
            ]);

            foreach ($orders as &$order) {
                $order  = collect($order);
                $that->tagOrder($request, $myShopify, $client, $order);
                $count = $count + 1;
            }

            if($count <= 20 ) {
                call($that, $myShopify, $request, $client, $count, collect($orders)->last()->id);
            }

            else {
                return [
                    'count' => $count,
                    'last' => collect($orders)->last()->id
                ];
            }
        }

        call($that, $myShopify, $request, $client, $count, false);
    }
}

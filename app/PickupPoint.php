<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PickupPoint extends Model
{
	protected $fillable = ['label', 'slug', 'postcodes'];

	public static function getPickupPoints()
	{
		return static::get();
	}

	public static function getAllPostcodes()
	{
		$postcodes = static::get()->reduce(function ($carry, $item) {
			return $carry.$item->postcodes.',';
		});
		return array_filter(explode(",", $postcodes));
	}

	public static function getPostcodes(string $slug)
	{
		return static::where('slug', $slug)->first()->value;
	}
}

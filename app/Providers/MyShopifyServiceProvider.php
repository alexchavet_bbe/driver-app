<?php

namespace App\Providers;

use App\Setting;
use App\MyShopify;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class MyShopifyServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
				app(MyShopify::class)
						->setShopUrl(Setting::getSetting('shopurl'))
            ->setAccessToken(Setting::getSetting('accesstoken'));
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
			$this->app->singleton(MyShopify::class, function ($app) {
					return new MyShopify(new Client);
			});
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $fillable = [
        'name',
        'address1',
        'address2',
        'city',
        'country',
        'company',
        'province',
        'zip',
        'province',
        'phone'
    ];

    public static function getPickupPoints()
    {
        return static::get();
    }

}

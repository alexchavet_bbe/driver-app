<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class Setting extends Model
{

		protected $fillable = ['label', 'value'];

		public static function initSetting()
		{
			$shopurl = new Setting;
			$shopurl->label = 'shopurl';
			$shopurl->value = 'localorganicdelivery-com-au.myshopify.com';
			$shopurl->save();

			$accesstoken = new Setting;
			$accesstoken->label = 'accesstoken';
			$accesstoken->value = env("SHOPIFY_TOKEN", null);
			$accesstoken->save();
		}

		public static function getSetting(string $label)
		{
			$setting =  Setting::where('label', $label)->first()->value;
			return $setting;
		}

		public static function getDate()
		{
			$offset_date = '4';
			$today_date = date("2018-02-03");
			$today_date =  Setting::where('label', 'date')->first()->value;
			$today_minus = date('Y-m-d', strtotime($today_date.' +'.$offset_date.' days'));
			$today_date = new DateTime($today_date);
			$today_minus = new DateTime($today_minus);
			$weekNumber = $today_date->format("W");
			$localWeekNumber = $today_minus->format("W");
			$local_delivery_week_number = $localWeekNumber - 1;
			$currentDay = $today_date->format("D");
			$date_min = date('Y-m-d',strtotime($today_minus->format("Y").'W'.($local_delivery_week_number)));
			$date_min = date('Y-m-d', strtotime($date_min.' -'.($offset_date-1).' days'));
			$date_min_exceptions = date('Y-m-d', strtotime($date_min.' -2 weeks'));
			$date_min = new DateTime($date_min);
			$date_min_exceptions = new DateTime($date_min_exceptions);


			$date_max =   Setting::where('label', 'cutOffDate')->first()->value;
			$date_max =  new DateTime($date_max);

			return [
				'date_min' =>  $date_min,
				'today_date' =>  $today_date,
				'date_min_exceptions' =>  $date_min_exceptions,
				'date_max' =>  $date_max
			];
		}
}

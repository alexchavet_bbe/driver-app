
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.Bus = new Vue({});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('total-container', require('./components/TotalContainer.vue'));
Vue.component('total-component', require('./components/TotalComponent.vue'));
Vue.component('order-container', require('./components/OrderContainer.vue'));
Vue.component('order-component', require('./components/OrderComponent.vue'));
Vue.component('routes-container', require('./components/RoutesContainer.vue'));
Vue.component('order-component', require('./components/OrderComponent.vue'));
Vue.component('optimo-container', require('./components/OptimoContainer.vue'));
Vue.component('loading-button', require('./components/LoadingButton.vue'));
Vue.component('customer-login', require('./components/customerLogin.vue'));
Vue.component('fulfillments-timeline', require('./components/tracking/fulfillmentsTimeline.vue'));
Vue.component('scheduling-info', require('./components/tracking/schedulingInfo.vue'));
Vue.component('order-status', require('./components/tracking/orderStatus.vue'));

Vue.component('setting-update', require('./components/settings/SettingUpdate.vue'));
Vue.component('pickup-point-update', require('./components/settings/PickupPointUpdate.vue'));
Vue.component('schools-update', require('./components/settings/SchoolsUpdate.vue'));

Vue.component('recharge-address-update', require('./components/recharge/AddressUpdate.vue'));
Vue.component('recharge-address-list', require('./components/recharge/AddressList.vue'));

const app = new Vue({
    el: '#app'
});

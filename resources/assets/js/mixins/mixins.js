const getOptimoId = function(id,number) {
	const value = id+'#'+number;
	return value
}

const getOrderId = function(id) {
	const value = id.split('#');
	return value[0]
}

const getOrderNumber = function(id) {
	const value = id.split('#');
	return value[1]
}

const formatAddress = function(a) {
	if (a.address2) {
		return `${a.address2} ${a.address1}, ${a.city}, ${a.zip}`;
	}
	return `${a.address1}, ${a.city}, ${a.zip}`;
}

export {getOptimoId,getOrderId, getOrderNumber, formatAddress};

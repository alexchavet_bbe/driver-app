@extends('layouts.app')

@section('title', 'Settings')

@section('sidebar')
    @parent
@endsection

@section('content')
	<div class="content">
		<div class="row">
			<div class="col">
		    <optimo-container/>
			</div>
		</div>
	</div>
@endsection

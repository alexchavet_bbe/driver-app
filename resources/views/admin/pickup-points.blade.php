@extends('layouts.app')

@section('title', 'Settings')

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="content">
	<pickup-point-update :pickup-points="{{ $pickup_points }}"></pickup-point-update>
	<form action="{{ route('pickup-point.store') }}" method="post">
			 {{ csrf_field() }}

	  <div class="row">
	    <div class="col">
	      Add:
	    </div>
	    <div class="col">
	      <input class="form-control"  type="text" name="label" placeholder="label"/>
	    </div>
	    <div class="col">
	      <input class="form-control"  type="text" name="slug" placeholder="slug"/>
	    </div>
	    <div class="col">
	      <input class="form-control"  type="text"  name="postcodes" placeholder="postcodes"/>
	    </div>
			<div class="col">
	      <button class="btn btn-sm btn-primary" type="submit" value="Add">Add Setting</button>
	    </div>
    </div>
		<div class="row">

	  </div>
	</form>
</div>
@endsection

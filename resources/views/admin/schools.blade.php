@extends('layouts.app')

@section('title', 'Settings')

@section('sidebar')
    @parent
@endsection

@section('content')
    <div class="content">
        <schools-update :schools="{{ $schools }}"></schools-update>
        <h3>Add new school</h3>
        <form action="{{ route('school.store') }}" method="post">
            {{ csrf_field() }}

            <div class="row">
                @foreach($fields as $key => $field)
                    @if($key !== 'id' && $key !== 'created_at' && $key !== 'updated_at')
                        <div class="col-6">
                            <label for="add_new_{{ $key }}">{{ $key }} <br>
                                <input id="add_new_{{ $key }}"
                                       class="form-control"
                                       type="text" name="{{ $key }}">
                            </label>
                        </div>
                    @endif
                @endforeach
            </div>
            <button type="submit">Add</button>
        </form>
    </div>
@endsection

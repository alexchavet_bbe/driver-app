@component('mail::message')
# Hi {{ $customer['first_name']  }},

To edit your subscription, you can log into your account using the link below:
@component('mail::button', ['url' => 'https://account.localorganicdelivery.com.au/customer/' . $customer['hash']])
 Manage Subscription
@endcomponent

Thanks,<br>
Local Organic Delivery
@endcomponent

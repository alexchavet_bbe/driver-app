<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<a class="navbar-brand" href="#"></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active">
				<a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="/drivers/routes">Drivers</a>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Recap
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="/recap/items">Items</a>
					<a class="dropdown-item" href="/recap/orders">Orders</a>
				</div>
			</li>
		</ul>

		<ul class="navbar-nav mr-auto">
			@if (Auth::check())
			<li class="nav-item dropdown">
		    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Admin</a>
		    <div class="dropdown-menu">
		      <a class="dropdown-item" href="/admin/optimo-create">Optimo</a>
		      <a class="dropdown-item" href="/admin/settings">Settings</a>
		      <a class="dropdown-item" href="/admin/pickup-points">Pickup Points</a>
		      <a class="dropdown-item" href="/admin/schools">Schools</a>
		    </div>
		  </li>
			@else
			<li class="nav-item active">
				<a class="nav-link" href="/login">Login<span class="sr-only">(current)</span></a>
			</li>
			@endif
		</ul>
	</div>
</nav>

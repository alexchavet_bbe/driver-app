<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<a class="navbar-brand" href="{{ route('public-tracking-index', $customer->id ) }}">Local Tracking</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
		<ul class="navbar-nav mr-0">
			<li class="nav-item active">
				<a class="btn btn-sm btn-primary" href="{{ route('public-tracking-index', $customer->id ) }}">
					See your orders <span class="sr-only">(current)</span></a>
			</li>
		</ul>
	</div>
</nav>

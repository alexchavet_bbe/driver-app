@extends('layouts.public')

@section('title', 'Settings')

@section('sidebar')
    @parent
@endsection
@section('header')
	<div class="row">
		<div class="col-12 col-md-6">
			<div class="jumbotron pl-0">
				<h1>Hi {{$customer->first_name}} {{$customer->last_name}},</h1>
				<p class="lead">
					Welcome to order tracking. This is where you check your delivery ETA.
				</p>
			</div>
		</div>
		<div class="col-12 col-md-6 col-bg">

		</div>
	</div>
@endsection
@section('content')
	<div class="content">
				<div class="row">
					<div class="col">
						<h3 class="mb-4">Your orders</h3>
					</div>
				</div>
				<div class="top-right links">
					<div class="row">
						<div class="col">
							<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										{{--<th scope="col">Status</th>--}}
										<th scope="col">Date</th>
										<th scope="col">Tracking</th>
									</tr>
								</thead>
								<tbody>
									@forelse ($orders as $order)
										<tr>
											{{--<td>{{$order->fulfillment_status}}</td>--}}
											<td>{{ Carbon\Carbon::parse($order->delivery_date)->format('l, F j Y') }}</td>
											<td>
												<a
													href="{{route('public-tracking-show', ['customer' => $customer->id, 'order' => $order->id])}}"
													class="btn btn-primary btn-sm">
														{{$order->fulfillment_status ? 'Delivered' : 'Tracking'}}
												</a>
											</td>
										</tr>
									@empty
									    <p>No users</p>
									@endforelse
								</tbody>
							</table>
							</div>
						</div>
					</div>
				</div>

	</div>
@endsection

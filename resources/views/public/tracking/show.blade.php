@extends('layouts.public')

@section('title', 'Settings')

@section('sidebar')
    @parent
@endsection
@section('header')
	<div class="row">
		<div class="col-12 col-md-6">
			<div class="jumbotron pl-0">

				<order-status :fulfillment-status="{{ $order['fulfillment_status'] ? 'true' : 'false'}}"></order-status>

			</div>
		</div>
		<div class="col-12 col-md-6 col-bg">

		</div>
	</div>
@endsection
@section('content')
	<div class="content">
		<div class="row">
			<div class="col">
				<h3 class="mb-4">More about your delivery</h3>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<scheduling-info
						order-id="{{$order['id']}}"
						:fulfillment-status="{{ $order['fulfillment_status'] ? 'true' : 'false'}}"
				></scheduling-info>
			</div>
		</div>
	</div>
@endsection

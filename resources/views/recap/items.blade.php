@extends('layouts.app')

@section('title', 'Settings')

@section('sidebar')
    @parent
@endsection

@section('content')
	<div class="content">
		<div class="row">
			<div class="col">
				<h1 class="page-title">Items totals</h1>
		    <total-container/>
			</div>
		</div>
	</div>
@endsection

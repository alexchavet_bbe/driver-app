@extends('layouts.app')

@section('title', 'Settings')

@section('sidebar')
    @parent
@endsection

@section('content')
	<div class="content">
		<div class="row">
			<div class="col">
				<h1 class="page-title">Order recap</h1>
		    <order-container/>
			</div>
		</div>
	</div>
@endsection

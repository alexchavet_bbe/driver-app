@extends('layouts.app')

@section('title', 'Settings')

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="content">
	<recharge-address-list :customers="{{ $customers }}"></recharge-address-list>
</div>
@endsection

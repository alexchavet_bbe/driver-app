@extends('layouts.app')

@section('title', 'Settings')

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="content">
	<setting-update :settings="{{ $settings }}"></setting-update>
	<form action="{{ route('setting.store') }}" method="post">
			 {{ csrf_field() }}

	  <div class="row">
	    <div class="col">
	      Add:
	    </div>
	    <div class="col">
	      <input class="form-control"  type="text" name="label" placeholder="label"/>
	    </div>
	    <div class="col">
	      <input class="form-control"  type="text"  name="value" placeholder="value"/>
	    </div>
			<div class="col">
	      <button class="btn btn-sm btn-primary" type="submit" value="Add">Add Setting</button>
	    </div>
    </div>
		<div class="row">

	  </div>
	</form>
</div>
@endsection

@extends('layouts.home')

@section('title', 'Settings')

@section('sidebar')
    @parent
@endsection
@section('header')
    <div class="row">
        <div class="col-12 col-md-6">
            <div class="jumbotron pl-0">
                <h1>Welcome to Local tracking</h1>
                <p class="lead">Enter your email address to track your orders</p>
            </div>
        </div>
        <div class="col-12 col-md-6 col-bg">

        </div>
    </div>
@endsection
@section('content')
    <div class="row text-left justify-content-center mb-4">
        <div class="col-12 col-md-4">
            <h4 class="mb-3">Please enter your order email</h4>
            <customer-login></customer-login>
        </div>
    </div>
    {{--<div class="row text-left justify-content-center mb-4">--}}
        {{--<div class="col-12 col-md-4">--}}
            {{--<h4 class="mb-3">I'am a Local driver</h4>--}}
            {{--<a href="/login" class="btn btn-primary btn-lg">Login</a>--}}
        {{--</div>--}}
    {{--</div>--}}

@endsection

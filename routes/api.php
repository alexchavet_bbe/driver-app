<?php

use App\Mail\CustomerPortalLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('fulfillments', 'ShopifyController@getFulfillments');
Route::get('fulfillments/updateETA', 'FulfillmentController@updateETA');
Route::get('fulfillments/{id}', 'FulfillmentController@index');
Route::get('fulfillments/search/{column}/{value}', 'FulfillmentController@search');
Route::get('fulfillments/summarize/{id}', 'FulfillmentController@summarize');

Route::get('fulfillments/fix/{id}', 'FulfillmentController@fix');

Route::post('fulfill', 'ShopifyController@fulfill');
Route::post('unfulfill', 'ShopifyController@unfulfill');

Route::put('update-order', 'ShopifyController@updateOrder');

Route::get('items', 'ShopifyController@getItems');

Route::get('recharge/customers/', 'RechargeController@getCustomers');
Route::get('recharge/customers/{id}', 'RechargeController@getCustomer');
Route::get('recharge/customers/shopify/{id}', 'RechargeController@getShopifyCustomer');

Route::get('recharge/customers/{id}/addresses/', 'RechargeController@getCustomerAddresses');
Route::get('recharge/customers/{c_id}/addresses/{a_id}', 'RechargeController@getCustomerAddress');
Route::put('recharge/address/update/', 'RechargeController@updateCustomerAddress');
Route::get('recharge/customers/addresses/', 'RechargeController@listCustomersAddresses');

// Route::get('recharge/subscriptions/', 'RechargeController@getCustomers');
// Route::get('recharge/subscriptions/{id}', 'RechargeController@getCustomer');
// Route::get('recharge/subscriptions/shopify/{id}', 'RechargeController@getShopifyCustomer');

Route::get('order/{id}', 'ShopifyController@getOrder');
Route::get('orders', 'ShopifyController@getOrders');
Route::get('orders/delivery', 'ShopifyController@getDeliveryOrders');
Route::get('products', 'ShopifyController@getProducts');
Route::get('orders-exceptions', 'ShopifyController@getOrdersExceptions');

Route::post('customer-id', 'ShopifyController@getCustomerId')->name('customer.id');

Route::post('optimo/test-order', 'OptimoRouteController@testOrder');
Route::post('optimo/create-order', 'OptimoRouteController@postRequestOrder');
Route::get('optimo/create-orders', 'OptimoRouteController@postOrders');
Route::get('optimo/delete-orders', 'OptimoRouteController@deleteOrders');
Route::get('optimo/get-schedule/{id}', 'OptimoRouteController@getSchedule');
Route::get('optimo/get-routes', 'OptimoRouteController@getRoutes');

Route::post('settings/create', 'SettingController@store')->name('setting.store');
Route::put('settings/update/{id}', 'SettingController@update')->name('setting.update');
Route::get('settings/delete/{id}', 'SettingController@delete')->name('setting.delete');


Route::post('school/create', 'SchoolController@store')->name('school.store');
Route::put('school/update/{id}', 'SchoolController@update')->name('school.update');
Route::get('school/delete/{id}', 'SchoolController@delete')->name('school.delete');


Route::post('pickup-point/create', 'PickupPointController@store')->name('pickup-point.store');
Route::put('pickup-point/update/{id}', 'PickupPointController@update')->name('pickup-point.update');

Route::get('webhook/order/all', 'WebhookController@orderAll')->name('webhook.order.all');
Route::post('webhook/order/created', 'WebhookController@orderCreated')->name('webhook.order.created');


Route::middleware('cors')->group(function(){
    Route::post('school/checkout/create', 'SchoolCheckoutController@create')->name('school.checkout.create');
    Route::get('school/index', 'SchoolController@index')->name('school.index');
});


Route::get('portal/customer/{id}', 'CustomerPortalController@get_customer');
Route::post('portal/customer/card', 'CustomerPortalController@update_card');
Route::get('portal/getallcollection', 'CustomerPortalController@get_all_collection');
Route::get('portal/collectionlist', 'CustomerPortalController@collectionList');

Route::post('portal/customer/{id}', 'CustomerPortalController@put_customer');
Route::get('portal/customer/{id}/payment_sources', 'CustomerPortalController@index_payment');
Route::get('portal/customer/{id}/charges', 'CustomerPortalController@index_charges');
Route::get('portal/customer/{id}/orders', 'CustomerPortalController@index_orders');
Route::get('portal/customer/{id}/address', 'CustomerPortalController@index_address');
Route::get('portal/customer/{id}/subscription', 'CustomerPortalController@index_subscriptions');
Route::get('portal/addresses/{id}', 'CustomerPortalController@get_address');
Route::post('portal/addresses/{id}', 'CustomerPortalController@put_address');
Route::get('portal/shopify_products/', 'CustomerPortalController@index_shopify_products');
Route::get('portal/products/', 'CustomerPortalController@index_products');
Route::delete('portal/onetimes/{id}', 'CustomerPortalController@delete_one_time');
Route::post('portal/onetimes/address/{id}', 'CustomerPortalController@add_one_time');
Route::get('portal/onetimes/address/{id}', 'CustomerPortalController@index_one_time');

Route::post('portal/subscriptions', 'CustomerPortalController@add_subscription');
Route::post('portal/subscriptions/{id}/set_next_charge_date', 'CustomerPortalController@set_next_charge_date');
Route::post('portal/subscriptions/{id}', 'CustomerPortalController@update_subscription');
Route::delete('portal/subscriptions/{id}', 'CustomerPortalController@delete_subscription');
Route::post('portal/addresses/{id}/subscriptions-bulk', 'CustomerPortalController@update_bulk_subscription');

Route::get('portal/collection/{id}', 'CustomerPortalController@get_collection');
Route::post('portal/checkout', 'CustomerPortalController@create_checkout');
Route::put('portal/checkout/{id}', 'CustomerPortalController@update_checkout');
Route::get('portal/checkout/{id}', 'CustomerPortalController@retrieve_checkout');
Route::get('portal/customer/{id}/tracking', 'TrackingAPIController@index');

Route::get('portal/link/preview', function (Request $request) {
    $response = app('App\Http\Controllers\RechargeController')->getCustomers();

    if(!count($response['customers'])) {
        return response('Error', 404);
    }

    $customer = $response['customers'][0];

    return new CustomerPortalLink($customer);
});

Route::post('portal/link', function (Request $request) {
    $validator = Validator::make($request->all(), [
        'email' => 'required'
    ]);

    if ($validator->fails()) {
        return response('Email required', 402);
    }

    $response = app('App\Http\Controllers\RechargeController')->getCustomer("?email=" . $request->get('email'));

    if(!count($response['customers'])) {
        return response()->json(['error' => 'Email not Found!'], 200);
    }

    if(count($response['customers'])) {
        $customer = $response['customers'][0];

        try {
            Mail::to($customer['email'])->send(new CustomerPortalLink($customer));
            return response('Your personalised account link will be sent shortly', 200);
        } catch (Exception $e) {
            return response($e, 500);
        }
    }

    return response()->json($request->get('email'));
});

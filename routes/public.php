<?php

Route::get('tracking/{customer}/{order}', 'TrackingController@show')->name('public-tracking-show');
Route::get('tracking/{customer}', 'TrackingController@index')->name('public-tracking-index');
Route::post('customer-id/{email}', 'ShopifyController@getCustomerId')->name('public-customer-id');

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin/recharge', 'RechargeController@index')->middleware('auth');
Route::get('/admin/settings', 'SettingController@index')->middleware('auth');
Route::get('/admin/pickup-points', 'PickupPointController@index')->middleware('auth');
Route::get('/admin/schools', 'SchoolController@show')->middleware('auth');

Route::get('/admin/optimo-create', function () {
    return view('admin/optimo-create-orders');
})->middleware('auth');

Route::get('/recap/items', function () {
    return view('recap.items');
})->middleware('auth');
Route::get('/recap/orders', function () {
    return view('recap.orders');
})->middleware('auth');
Route::get('/drivers/route', function () {
    return view('drivers.route');
})->middleware('auth');
Route::get('/drivers/routes', function () {
		return view('drivers.routes', [
			'page-title'  => 'Drivers'
		]);
})->middleware('auth');


Route::get('/about', function () {

		$shopUrl = "localorganicdelivery-com-au.myshopify.com";
		$accessToken = "46c7bbc88a43ff0647d441e09e385fe0";
		$products = Shopify::setShopUrl($shopUrl)->setAccessToken($accessToken)->get("admin/products.json");

		dd($products);

    return view('about', [
			'name' => 'Alex',
			'products' => $products,
			'date' => 0
		]);
})->middleware('auth');

use Oseintow\Shopify\Facades\Shopify;
use App\Setting;

Route::get("init_shop",function()
{
  	$shopify = Setting::initSetting();
});

Route::get("install_shop",function()
{
    $shopUrl = "localorganicdelivery-com-au.myshopify.com";
    $scope =  env("SHOPIFY_SCOPES", null);
    $redirectUrl = "http://driver-app.bbe/process_oauth_result";

    $shopify = Shopify::setShopUrl($shopUrl);
    return redirect()->to($shopify->getAuthorizeUrl($scope,$redirectUrl));
});

Route::get("process_oauth_result",function(\Illuminate\Http\Request $request)
{
    $shopUrl = "localorganicdelivery-com-au.myshopify.com";
    $accessToken = Shopify::setShopUrl($shopUrl)->getAccessToken($request->code);

    dd($accessToken);

    // redirect to success page or billing etc.
});




Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/customers', 'CustomerPortalController@index_customer');


Route::get('/debug-sentry', function () {
    throw new Exception('My first Sentry error!');
});
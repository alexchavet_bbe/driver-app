<?php

namespace Tests\Feature;

use Oseintow\Shopify\Facades\Shopify;
use Tests\CreatesApplication;
use Tests\TestCase;

class TestTagCustomerTest extends TestCase
{
    use CreatesApplication;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    protected $shopify;

    public function __construct()
    {

    }

    public function testGetOrders()
    {
        $shopUrl = "localorganicdelivery-com-au.myshopify.com";
        $this->shopify = Shopify::setShopUrl($shopUrl);

        $id = 2119774765174;
        $response = $this->shopify->get('admin/orders/' . $id . '.json', '');
        $response->assertStatus(200);
    }
}

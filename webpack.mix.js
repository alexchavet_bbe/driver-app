let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.copyDirectory('resources/assets/images', 'public/images');
mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
	 mix.browserSync({
	         proxy: 'http://driver-app.bbe/',
	         files: ["public/js", "public/css"]
	     });
	 // mix.js([
	 //     'node_modules/jquery/dist/jquery.min.js',
	 //     'node_modules/bootstrap/dist/js/bootstrap.js',
	 //     'resources/assets/js/app.js'], 'public/js');
	 // mix.sass('resources/assets/sass/app.scss', 'public/css'); /* If you want to make admin css file. */

	 // version does not work in hmr mode
	 if (process.env.npm_lifecycle_event !== 'hot') {
	   mix.version()
	 }
	 const path = require('path');
	 // fix css files 404 issue
	 mix.webpackConfig({
	   devServer: {
	     contentBase: path.resolve(__dirname, 'public'),
	   }
	 });
